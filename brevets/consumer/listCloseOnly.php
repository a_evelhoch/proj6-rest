<html>
    <head>
        <title>Brevets RESTful API: listCloseOnly examples</title>
    </head>

    <body>
        <h1>Responses from listCloseOnly API paths</h1>
        <hr>
        <h2>Response from: ./listCloseOnly</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listCloseOnly');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly/csv</h2>
        <ul>
            <?php
            $string = file_get_contents('http://flask-server/listCloseOnly/csv');
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly/json</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listCloseOnly');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listCloseOnly?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly/json?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listCloseOnly/json?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly/csv?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $string = file_get_contents("http://flask-server/listCloseOnly/csv?top=$int");
            echo "Where k is $int";
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
    </body>
</html>
