"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request, flash
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import mongo_handler # My .py I wrote to handle the mongodb stuff
import config
import json
import logging
import os

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

api = Api(app)



###
# API
###

# API Resources

class listAll(Resource):
    """
    The listAll resource grabs the top x items from the sorted list and returns them in json format,
    where x is the value passed in using the ?top=x query parameter. If no query parameter is specified,
    it will simply return all of the items in the sorted list.
    """
    def get(self):
        topArg = request.args.get('top', '')   #| Get the 'top' query parameter if it exists.
        if (topArg):                           #| If it does exist, then 
            try:                               #| we use this try to check if its an int
                int(topArg)                    #| this fails if the 'top' parameter isnt a number
                getTop = int(topArg)           #| if it was a number, set it as our number of objects to get.
            except ValueError:                 #| if the parameter isn't a number, we go here
                getTop = 20                    #| and just default to getting all 20 entries.
        else: getTop = 20                      #| If the query parameter doesn't exist, just default to getting all 20 entries.
        data = mongo_handler.get_data(getTop) # Then finally get the entries
        if(data):                       #| If we actually got any data,
            return flask.jsonify(data)  #| then return it.
        else:                           #| Otherwise,
            return 'No data'            #| return 'No data'

class listAllArgs(Resource):
    """
    This is a version of the listAll resource that takes another argument like this: listAll/<argument>
    If the argument is 'json' it will return the top x items in json format, and if the argument is 'csv'
    it will return the top x items in csv format, where x is the value passed in using the ?top=x query paramenter.
    If no parameter is specified, it will simply return all of the items in the sorted list.
    """
    def get(self, format_arg):                 #| This is just the same as the first |ed comment as the non format_arg version
        topArg = request.args.get('top', '')   #| I won't be commenting repeated lines here
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()        # Get the part after listAllArgs/<here> and convert it to uppercase so we can compare w/o capitalization mattering
        data = mongo_handler.get_data(getTop)  # More repeated code
        if(data):
            if(format_arg == "JSON"):       # If the format argument is JSON, then it just returns the object in JSON format as usual
                return flask.jsonify(data)
            elif(format_arg == "CSV"):      # Otherwise, we need to return it in csv format
                csvString = "Distance, Opening, Closing" + '\n'  # The first line is a template for how the data is formatted
                for line in data:                                # Then we loop through each entry in data and add it to the csv string in comma-seperated format
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["opening"]) + ", " + str(line["closing"]) + '\n'
                return csvString    # And finally, we return the csv data
            else:
                return "Invalid format"  # Otherwise, we return "Invalid format"
        else:
            return 'No data'

class listOpenOnly(Resource):
    """
    This is a copy of listAll except it removes the 'closing' value before returning data
    """
    def get(self):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'closing' in element: element.pop('closing', None)  # Loop through the data and remove the "closing" elements
            return flask.jsonify(data)
        else:
            return 'No data'

class listOpenOnlyArgs(Resource):
    """
    This is a copy of listAllArgs except it removes the 'closing' value before returning data
    """
    def get(self, format_arg):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'closing' in element: element.pop('closing', None)
            if(format_arg == "JSON"):
                return flask.jsonify(data)
            elif(format_arg == "CSV"):
                csvString = "Distance, Opening" + '\n'
                for line in data:
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["opening"]) + '\n'
                return csvString
            else:
                return "Invalid format"
        else:
            return 'No data'

class listCloseOnly(Resource):
    """
    This is a copy of listAll except it removes the 'opening' value before returning data
    """
    def get(self):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'opening' in element: element.pop('opening', None)  # Loop through the data and remove the "opening" elements
            return flask.jsonify(data)
        else:
            return 'No data'

class listCloseOnlyArgs(Resource):
    """
    This is a copy of listAllArgs except it removes the 'opening' value before returning data
    """
    def get(self, format_arg):
        topArg = request.args.get('top', '')
        if (topArg):
            try:
                int(topArg)
                getTop = int(topArg)
            except ValueError:
                getTop = 20
        else: getTop = 20
        format_arg = format_arg.upper()
        data = mongo_handler.get_data(getTop)
        if(data):
            for element in data:
                if 'opening' in element: element.pop('opening', None)
            if(format_arg == "JSON"):
                return flask.jsonify(data)
            elif(format_arg == "CSV"):
                csvString = "Distance, Closing" + '\n'
                for line in data:
                    if(line["distance"] != ''): csvString = csvString + str(line["distance"]) + ", " + str(line["closing"]) + '\n'
                return csvString
            else:
                return "Invalid format"
        else:
            return 'No data'

# API routes

api.add_resource(listAll, '/listAll')
api.add_resource(listAllArgs, '/listAll/<format_arg>')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listOpenOnlyArgs, '/listOpenOnly/<format_arg>')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listCloseOnlyArgs, '/listCloseOnly/<format_arg>')

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    getDate = request.args.get('date')
    getTime = request.args.get('time')
    brevet_dist_total = int(request.args.get('total_dist'))
    timeString = getDate + " " + getTime
    # Looks like "2000-06-10 12:00"
    formattedTime = arrow.get(timeString, 'YYYY-MM-DD HH:mm')
    formattedTime = formattedTime.replace(tzinfo='US/Pacific')
    formattedTime = formattedTime.isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist_total, formattedTime)
    close_time = acp_times.close_time(km, brevet_dist_total, formattedTime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit_values")
def _submit_values():
    """
    This function takes the data given to it from calc.html's submit_values() function, which is in the form of
    values read from the html page paired with a distance, open time or close time. It then attempts to iterate 
    through them if and adds each three value row to the mongodb. It then responds that it succeeded in adding them.
    If no data is recieved from calc.html, then it responds that there was an error.
    """
    result = {"printme" : "Error: No data entered."}          # Prepare the base case response first in case nothing happens
    if (request.args.get('control_point_distance_1') != ''):  #| Then if the function got any data at all then we're replacing the 
        mongo_handler.reset()                                 #| current data with it, so delete it all.
    for i in range(20):     # Then iterate through the data
        iterator = i + 1    # Increase the iterator since the data from calc.html is 1-indexed
        tempDist = request.args.get('control_point_distance_' + str(iterator))  # Put each arguement from that # iterator into a temp var
        if(tempDist != ''): tempDist = int(tempDist) ############### added this ################################
        tempOpen = request.args.get('open_time_' + str(iterator))
        tempClose = request.args.get('close_time_' + str(iterator))
        if (tempDist != ''):  # Then if that variable is not empty, that means the row has data in it
            result.update({"printme" : "Saved to database"})     #| And we can add it to the mongodb. Also, change the response
            mongo_handler.add_row(tempDist, tempOpen, tempClose) #| so it says we saved data because we've done that at least once
    return flask.jsonify(result=result)  # And return the response for calc.html to print

@app.route("/_return_values")
def _return_values():
    """
    This function gets the data from the mongodb (if there is any) and returns it to calc.html's display_saved() function
    so that that function can display it. The data comes from mongodb in a list of sets of three key-value pairs for 
    distance, open, and close times. If there is no data saved in the db, then it returns an error for calc.html to
    display.
    """
    data = mongo_handler.get_data(20)     # Take all the data saved in the mongodb and put it into data.
    if (data):  # If there's data in the db
        return flask.jsonify(result=data) # Then return it to the js function in calc.html so that it can display it
    else:       # If there's no data in the db
        result = {"printme" : "Error: No data saved."}  # Return an error to the js in calc.html so that it can display it.
        return flask.jsonify(result=result)
    return "No clue how this happened Lol"  # Catchall case just in case. This shouldnt happen.

@app.route("/_delete_values")
def _delete_values():
    """
    This function tells the mongodb to delete all the data in the collection. It then returns a message saying if it
    succeeded or failed for calc.html to display. This function is not strictly neccessary for the project, but it makes
    testing empty database responses easier, since the mongodb container will save the data between builds.
    """
    data = mongo_handler.get_data(20)
    if (data):
        mongo_handler.reset()
        result = {"printme" : "Saved brevet deleted."}
    else:
        result = {"printme" : "Error: No data to delete."}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
