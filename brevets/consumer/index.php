<html>
    <head>
        <title>Brevets RESTful API test page</title>
    </head>

    <body>
        <h1>Brevets RESTful API test page</h1>
        Below is the result from various calls to the flask brevets API. Sections are seperated by horizontal lines, and at the top of each one it tells you the URL that the API call goes to. For the calls to the 'top k' parameters, it will call with a random number from 1 to 20. Simply refresh the page if you want to see different 'top k's.' <br />
        This page shows the smallest combination of outputs to show that each API path (listAll, listOpenOnly, listCloseOnly), each representation (json and csv) and the 'top k' query parameter works, if you want to see a more exhaustive demonstration of combinations, below are links to pages for each API path that demonstrate the possible combinations of representations and queries:<br />
        <a href="listAll.php">listAll API</a><br />
        <a href="listOpenOnly.php">listOpenOnly API</a><br />
        <a href="listCloseOnly.php">listCloseOnly API</a><br />
        <hr>
        <h2>Response from: ./listAll</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listAll');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly/csv</h2>
        <ul>
            <?php
            $string = file_get_contents('http://flask-server/listOpenOnly/csv');
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listCloseOnly/json?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listCloseOnly/json?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
    </body>
</html>
