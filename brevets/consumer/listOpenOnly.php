<html>
    <head>
        <title>Brevets RESTful API: listOpenOnly examples</title>
    </head>

    <body>
        <h1>Responses from listOpenOnly API paths</h1>
        <hr>
        <h2>Response from: ./listOpenOnly</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listOpenOnly');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly/csv</h2>
        <ul>
            <?php
            $string = file_get_contents('http://flask-server/listOpenOnly/csv');
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly/json</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listOpenOnly');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listOpenOnly?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly/json?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listOpenOnly/json?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listOpenOnly/csv?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $string = file_get_contents("http://flask-server/listOpenOnly/csv?top=$int");
            echo "Where k is $int";
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
    </body>
</html>
