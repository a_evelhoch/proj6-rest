<html>
    <head>
        <title>Brevets RESTful API: listAll examples</title>
    </head>

    <body>
        <h1>Responses from listAll API paths</h1>
        <hr>
        <h2>Response from: ./listAll</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listAll');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listAll/csv</h2>
        <ul>
            <?php
            $string = file_get_contents('http://flask-server/listAll/csv');
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listAll/json</h2>
        <ul>
            <?php
            $json = file_get_contents('http://flask-server/listAll');
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listAll?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listAll?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listAll/json?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $json = file_get_contents("http://flask-server/listAll/json?top=$int");
            echo "Where k is $int";
            $obj = json_decode($json);
            $counter = 1;
            foreach ($obj as $brevet) {
               echo "<h3>Brevet $counter:</h3><ul>";
               echo "<li>Distance = $brevet->distance</li>";
               echo "<li>Opening Time = $brevet->opening</li>";
               echo "<li>Closing Time = $brevet->closing</li>";
               echo "</ul>";
               $counter++;
            }
            ?>
        </ul>
        <hr>
        <h2>Response from: ./listAll/csv?top=k</h2>
        <ul>
            <?php
            $int = rand(1,20);
            $string = file_get_contents("http://flask-server/listAll/csv?top=$int");
            echo "Where k is $int";
            echo '<pre>';
            $string = str_replace(array("\r\n","\r","\\n"), "<br />", $string);
            echo substr($string, 1, -2);
            echo '</pre>';
            ?>
        </ul>
    </body>
</html>
